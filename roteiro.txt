git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init - Inicializa o git na pasta indicada.

git status - Verifica situação do repositorio

git add arquivo/diretório - Adiciona somente o arquivo descrito para a area de preparação
git add --all = git add . - Adiciona todos os arquivos para a area de preparação

git commit -m “Primeiro commit” - Salva as mudanças dos arquivos que estão na area de preparação no repositorio e adiciona o comentario Primeiro Commit

-------------------------------------------------------

git log - Mostra todas as alterações feitas no repositorio em ordem cronologica de forma detalhada.
git log arquivo - Mostra todas as alterações feitas no arquivo descrito em ordem cronologica de forma detalhada.
git reflog - Mostra a sequencia de todas as ações feitas com o repositorio, sendo possivel voltar para o momento de uma ação realizada caso necessario.

-------------------------------------------------------

git show - Mostra detalhadamente as alterações feitas pelo ultimo commit.
git show <commit> Mostra detalhadamente as alterações feitas em determindado commit.

-------------------------------------------------------

git diff - Mostra a diferença entre os ultimos dois commits 
git diff <commit1> <commit2> - Mostra a diferença entre os commits determinados

-------------------------------------------------------

git reset --hard <commit> - Desfaz uma mudança feita em um commit específico

-------------------------------------------------------

git branch - Lista todas as branchs locais
git branch -r - Lista todas as branchs remotas	
git branch -a - Lista todas as branchs, tanto as locais quanto as remotas
git branch -d <branch_name> - Remove determinada branch local 
git branch -D <branch_name> - Remove forçadamente determinada branch local
git branch -m <nome_novo> - - Renomeia a branch corrente
git branch -m <nome_antigo> <nome_novo> - Renomeia determinada branch local que não seja a corrente

-------------------------------------------------------

git checkout <branch_name> - Muda para uma outra branch já criada
git checkout -b <branch_name> - Cria e muda para uma nova branch

-------------------------------------------------------

git merge <branch_name> - Combina determinada branch na branch corrrente

-------------------------------------------------------

git clone - Clona um repositorio a partir do link para a pasta corrente
git pull - Atualiza o repositorio local com os dados do repositorio remoto
git push - Atualiza o repositorio remoto com os dados do repositorio local

-------------------------------------------------------

git remote -v - Vizualiza os repositorios remotos salvos
git remote add origin <url> - Salva um novo repositorio remoto origin.
git remote <url> origin - Altera a URL de um repositorio remoto já existente.

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLucm8g_ezqNq0dOgug6paAkH0AQSJPlIe

Vídeo sobre Git: 
https://www.youtube.com/watch?v=kB5e-gTAl_s


